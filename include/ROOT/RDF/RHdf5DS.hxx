#ifndef RDF_RHDF5DS_HH
#define RDF_RHDF5DS_HH

#include <Riostream.h>
#include <TObject.h>

namespace ROOT {
namespace RDF {
    class RHdf5DS : public TObject {
    public:
    
        RHdf5DS() ;
        virtual ~RHdf5DS();

        ClassDef(RHdf5DS, 1) // Required for ROOT dictionary
    };
} // namespace RDF
}
#endif // RDF_RHDF5DS_HH